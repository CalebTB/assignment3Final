# Copyright 2015 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# [START gae_flex_quickstart]
from flask import Flask, request


app = Flask(__name__)


@app.route("/")
def home():
    feet = request.args.get("height_feet", "")
    inches = request.args.get("height_inches", "")
    pounds = request.args.get("weight_pounds", "")
    if feet or inches or pounds:
        bmi_return = bmi(feet, inches, pounds)
    else:
        
        bmi_return = ""
    return (
        """<form action="" method="get">
                Feet: <input type="text" name="height_feet" required>
                Inches: <input type="text" name="height_inches" required>
                Weight in Pounds: <input type="text" name="weight_pounds" required>
                
                <input type="submit" value="Convert">
            </form>"""
        + "(BMI, Category): "
        + bmi_return
    )

def bmi(height_feet_in, height_inches_in, weight_pounds_in):
    # Convert input to float
    height_feet = float(height_feet_in)
    height_inches = float( height_inches_in)
    weight_pounds = float(weight_pounds_in)
    
    try:
    # Convert height to inches
        total_height_inches = (height_feet * 12) + height_inches

    # Metric Conversions
        height_metric_meters = total_height_inches * 0.025
        weight_metric_kg = weight_pounds * 0.45

    # Calculate BMI
        bmi_calc = round(weight_metric_kg / (height_metric_meters ** 2), 1)

    # Determine BMI category
        if bmi_calc < 18.5:
            category = "Underweight"
        elif 18.5 <= bmi_calc < 24.9:
            category = "Normal weight"
        elif 25 <= bmi_calc < 29.9:
            category = "Overweight"
        else:
            category = "Obese"

        bmi_string = str(bmi_calc) + " | " + category

        return bmi_string
    
    except ValueError:
        return "Invalid Input"


if __name__ == "__main__":
    # This is used when running locally only. When deploying to Google App
    # Engine, a webserver process such as Gunicorn will serve the app.
    app.run(host="127.0.0.1", port=8080, debug=True)
# [END gae_flex_quickstart]
