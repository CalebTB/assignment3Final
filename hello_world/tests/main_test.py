# Copyright 2015 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from hello_world.src.main import bmi


def test_bmi_calculations():
    actual_return = bmi(5, 3, 125)

    # Add assertions based on the expected values or ranges
    print(actual_return)
    assert actual_return == "22.7 | Normal weight"

def test_bmi_underweight():
    actual_return = bmi(5, 3, 65)

    print(actual_return)
    assert actual_return == "11.8 | Underweight"

    actual_return = bmi(1, 1, 1)

    print(actual_return)
    assert actual_return == "4.3 | Underweight"

    actual_return = bmi(5, 3, 101.5)

    print(actual_return)
    assert actual_return == "18.4 | Underweight"

def test_bmi_normalweight():
    actual_return = bmi(5, 3, 125)
    print(actual_return)
    assert actual_return == "22.7 | Normal weight"

    actual_return = bmi(5, 3, 102)
    print(actual_return)
    assert actual_return == "18.5 | Normal weight"

    actual_return = bmi(5, 3, 136.5)
    print(actual_return)
    assert actual_return == "24.8 | Normal weight"

def test_bmi_overweight():
    actual_return = bmi(5, 3, 155)
    print(actual_return)
    assert actual_return == "28.1 | Overweight"

    actual_return = bmi(5, 3, 138)
    print(actual_return)
    assert actual_return == "25.0 | Overweight"

    actual_return = bmi(5, 3, 164.5)
    print(actual_return)
    assert actual_return == "29.8 | Overweight"

def test_bmi_obese():
    actual_return = bmi(5, 3, 500)
    print(actual_return)
    assert actual_return == "90.7 | Obese"

    actual_return = bmi(5, 3, 165.5)
    print(actual_return)
    assert actual_return == "30.0 | Obese"